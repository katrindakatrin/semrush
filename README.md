# Preparing an application image

This guide provides step-by-step instruction for preparing an application *ExampleApp* image using Docker. It assumes that you have [Docker](https://docs.docker.com/engine/install/) installed on your system.

Our *ExampleApp* accepts requests on port `8000`. Here we consider preparing an image with it for later deployment on Kubernetes.

The project will have the following directory structure:

```
├── quickstart_docker/ # Project directory
│ ├── application/ # Application code directory
│ ├── docker/ # Docker-related files directory
│ ├── application/ # Dockerfile for the application 
```

To prepare an application image:

1. Create directories for the project:

	```bash
	mkdir -p quickstart_docker/{application,docker/application}
	```

2. Create an application file named `application.py` in the `quickstart_docker/application` directory.

3. Add the following code into it and save the file.
	
	```python
	import http.server
	import socketserver

	PORT = 8000

	Handler = http.server.SimpleHTTPRequestHandler

	httpd = socketserver.TCPServer(", PORT), Handler)

	print("serving at port", PORT)
	httpd.serve_forever()
	```
	
	This code sets up a basic HTTP server that listens on port `8000`.

4. Create a file named `Dockerfile` in the `quickstart_docker/docker/application` directory.

5. Copy the following contents into it and save.

	```dockerfile
	# Use base image from the registry
	FROM python:3.5

	# Set the working directory to /app
	WORKDIR /app

	# Copy the 'application' directory contents into the container at /app
	COPY ./application /app

	# Make port 8000 available to the world outside this container
	EXPOSE 8000

	# Execute 'python /app/application.py' when container launches
	CMD ["python", "/app/application.py"]

	```
	
	This Dockerfile specifies the base image, sets the working directory, copies the application code into the container, exposes port `8000`, and defines the command to run the application.
	
	> *Note.* For more, [see the article](https://docs.docker.com/engine/reference/builder/) about the Dockerfile.  

6. Navigate to the main project directory `quickstart_docker` and start the image creation process:

	```bash
	docker build . -f docker/application/Dockerfile -t exampleapp
	```

	Where `docker build` is the command to create the image.  
	* `.` — the build context. `.` means the current folder.  
	* `-f` — flag to indicate the path to `Dockerfile`. 
	
	* `docker/application/Dockerfile` - path to `Dockerfile`.  
	* `-t` — flag to denote the image tag. The tag is needed to make it easier to find the image in the list. 
	
	* `exampleapp` — image tag.

	**Command result example**

	```text
	=> [internal] load build definition from Dockerfile                                              0.0s
 	=> => transferring dockerfile: 414B                                                              0.0s
 	=> [internal] load .dockerignore                                                                 0.0s
 	=> => transferring context: 2B                                                                   0.0s
 	=> [internal] load metadata for docker.io/library/python:3.5                                     0.9s
 	=> [1/3] FROM docker.io/library/python:3.5@sha256:42a37d6b8c00b186bdfb2b620fa8023eb775b3eb3a768  0.0s
 	=> [internal] load build context                                                                 0.0s
	=> => transferring context: 79B                                                                  0.0s
 	=> [2/3] WORKDIR /app                                                                            0.0s
 	=> [3/3] COPY ./application /app                                                                 0.0s
 	=> exporting to image                                                                            0.0s
 	=> => exporting layers                                                                           0.0s
 	=> => writing image sha256:b1a89f88f0045685dbf256e07ebbc60b57c4cdec7c81243fdd1a90c1884e4ff3      0.0s
 	=> => naming to docker.io/library/exampleapp                                                     0.0s
	```

7. Execute the `docker images` command.

	**Command result example**

	```text
	REPOSITORY    TAG       IMAGE ID       CREATED        SIZE
	exampleapp    latest    b1a89f88f004   1 hours ago   871MB
	```

8. Ensure that the image named **exampleapp** is present in the output.


> *What's next?* The next step is to [send our image](https://www.semrush.com/example_link) to the repository.